-- Uses imagemagick and curl for expediency.

module Image where

import Users
import Config

import Data.Char
import System.Process
import System.Exit
import System.FilePath
import Crypto.Hash
import System.Directory
import System.IO
import System.Posix
import qualified Data.ByteString as B
import qualified Data.ByteString.UTF8 as U8

storeUserImage :: (FilePath -> IO Bool) -> IO (Maybe UserId)
storeUserImage getimage = do
	createDirectoryIfMissing True staticDir
	let lockfile = staticDir </> "lck"
	l <- openFd lockfile WriteOnly (Just stdFileMode) defaultFileFlags
	waitToSetLock l (WriteLock, AbsoluteSeek, 0, 0)
	fs <- getDirectoryContents staticDir
	let uid = findunuseduid fs [0..]
	r <- getimage (staticDir </> show uid)
	closeFd l
	if r
		then return (Just (UserId uid))
		else return Nothing
  where
	findunuseduid fs (x:xs)
		| show x `elem` fs = findunuseduid fs xs
		| otherwise = x
	findunuseduid _ _ = 42 -- impossible

downloadLibravatar :: String -> FilePath -> IO Bool
downloadLibravatar email dst = do
	let email' = map toLower email
	let h = show (md5 (U8.fromString email'))
	let url = "https://cdn.libravatar.org/avatar/"
		++ h ++ "?s=" ++ show maxImageSize ++ "&d=monsterid"
	ok <- downloadUrl url dst
	if ok
		then do
			sz <- getFileSize dst
			if sz == 0
				then do
					removeFile dst
					return False
				else return True
		else return False

md5 :: B.ByteString -> Digest MD5
md5 = hash

downloadAndScaleUrl :: String -> FilePath -> IO Bool
downloadAndScaleUrl url dst = do
	let tmp = (dst <.> "tmp")
	got <- downloadUrl url tmp
	if got
		then do
			r <- makeIcon tmp dst
			removeFile tmp
			return r
		else return False

downloadUrl :: String -> FilePath -> IO Bool
downloadUrl url dst = do
	p <- spawnProcess "curl"
		[ "--max-filesize", "10M", "-Lso", dst, "--", url]
	(== ExitSuccess) <$> waitForProcess p

makeIcon :: FilePath -> FilePath -> IO Bool
makeIcon = scaleImage maxImageSize

maxImageSize :: Int
maxImageSize = 512

allowedImageTypes :: [String]
allowedImageTypes = ["PNG", "GIF", "JEPG", "SVG"]

scaleImage :: Int -> FilePath -> FilePath -> IO Bool
scaleImage maxsz src dst = do
	(_,t,_) <- readCreateProcessWithExitCode
		(proc "identify" ["-format", "%m", src]) ""
	if t `elem` allowedImageTypes
		then do
			p <- spawnProcess "convert"
				[ src
				, "-resize"
				-- The ">" makes it only shrink large images,
				-- not scale up small.
				, show maxsz ++ "x" ++ show maxsz ++ ">"
				, dst
				]
			(== ExitSuccess) <$> waitForProcess p
		else return False
