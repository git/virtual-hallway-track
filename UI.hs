{-# LANGUAGE FlexibleInstances #-}

module UI where

import Users
import Config

import qualified Graphics.UI.Threepenny as UI
import Graphics.UI.Threepenny.Core
import Control.Concurrent.STM
import Control.Monad
import System.FilePath
import System.Directory
import Data.Function ((&))

loadImage :: User -> Int -> UI Element
loadImage (User { userId = UserId uid }) scaleto = do
	let p = staticDir </> show uid
	exists <- liftIO $ doesFileExist p
	let p' = if exists then p else staticDir </> "bot.png"
	img <- UI.loadFile "image" p'
	UI.img
		& set UI.src img
		& set UI.width scaleto
		& set UI.height scaleto
			

-- A sprite contains a threepenny Element, and remembers where it's
-- currently located.
data Sprite t = Sprite Element (TVar (Int, Int)) t

instance Show (Sprite UserId) where
	show (Sprite _ _ t) = "Sprite " ++ show t

spritePos :: Sprite t -> UI (Int, Int)
spritePos (Sprite _elt v _) = liftIO $ atomically $ readTVar v

spriteElement :: Sprite t -> Element
spriteElement (Sprite elt _ _) = elt

spriteVal :: Sprite t -> t
spriteVal (Sprite _ _ t) = t

mkSprite :: t -> (Int, Int) -> Element -> UI (Sprite t)
mkSprite t (x, y) elt = do
	v <- liftIO $ newTVarIO (x, y)
	_ <- moveTo (x, y) elt
	return (Sprite elt v t)

hideSprite ::Sprite t -> UI ()
hideSprite (Sprite elt _ _) = void $
	element elt # set style [("display", "none")]

showSprite ::Sprite t -> UI ()
showSprite (Sprite elt _ _) = void $ 
	element elt # set style [("display", "block")]

class Moveable t where
	moveTo :: (Int, Int) -> t -> UI t

instance Moveable Element where
	moveTo (x, y) elt = element elt # set style
		[ ("position", "absolute")
		, ("left", show x ++ "px")
		, ("top", show y ++ "px")
		]

instance Moveable (Sprite t) where
	moveTo (x, y) s@(Sprite elt v _) = do
		_ <- moveTo (x, y) elt
		liftIO $ atomically $ writeTVar v (x, y)
		return s

data ScreenSize = ScreenSize 
	{ screenWidth :: Int
	, screenHeight :: Int
	}

getScreenSize :: UI ScreenSize
getScreenSize = ScreenSize <$> getScreenWidth <*> getScreenHeight

getScreenWidth :: UI Int
getScreenWidth = callFunction $ ffi "window.innerWidth"

getScreenHeight :: UI Int
getScreenHeight = callFunction $ ffi "window.innerHeight"

getObjectWidth :: Element -> UI Int
getObjectWidth el = callFunction $ ffi "%1.offsetWidth" el

getObjectHeight :: Element -> UI Int
getObjectHeight el = callFunction $ ffi "%1.offsetHeight" el

getWindowUrl :: UI String
getWindowUrl = callFunction $ ffi "window.location"

setWindowUrl :: String -> UI ()
setWindowUrl url = callFunction $ ffi "window.location=%1" url

openWindow :: String -> UI ()
openWindow url = callFunction $ ffi "window.open(%1)" url

arrangeCircle :: Moveable t => Int -> (Int -> Int, Int -> Int) -> Double -> [(t, ((Int -> Int), (Int -> Int)))] -> UI [t]
arrangeCircle radius (fx, fy) skew l = do
	mapM (uncurry go) $
		zip (circlePoints radius (length l) skew) l
  where
	go (x, y) (t, (fx', fy')) = moveTo (fx' (fx x), fy' (fy y)) t

-- Points of a circle around the origin.
circlePoints :: Int -> Int -> Double -> [(Int, Int)]
circlePoints radius n skew = map point $ map (+ skew) steps
  where
	r = fromIntegral radius
	point theta = (floor (r * cos theta), floor (r * sin theta))
	stepsz = pi * 2 / fromIntegral n
	steps = take n $ map (stepsz *) [0..]

mkWall :: Bool -> ScreenSize -> UI Element
mkWall top sz = do
	d <- UI.div
	element d & set UI.style 
		[ ("background-color", "black")
		-- must be a way to make it dynamically resize,
		-- but short of javascript, I coud not find one,
		-- so pre-allocate space
		, ("width", "500%")
		, ("height", show h ++ "px")
		, ("position", "absolute")
		, ("left", "0px")
		, ("top", show y ++ "px")
		, ("margin", "0px")
		]
  where
	y = if top then 0 else screenHeight sz - h
	h = wallHeight sz

wallHeight :: ScreenSize -> Int
wallHeight sz = screenHeight sz `div` 10

mkSaying :: ScreenSize -> Bool -> UI Element
mkSaying sz highlighted = UI.div # set UI.style
	[ ("background-color", color)
	, ("width", show w ++ "px")
	, ("height", show h ++ "px")
	, ("position", "absolute")
	, ("margin", "0px")
	, ("border-radius",show corner ++ "px")
	, ("padding", "1px " ++ show (corner `div` 2) ++ "px")
	, ("z-index", "10") -- above faces
	, ("overflow-y", "scroll")
	]
  where
	h = faceSz sz
	w = sayingWidth sz
	corner = faceSz sz `div` 5
	color = if highlighted then "lightgreen" else "lightgrey"

data UserForm = UserForm
	{ userFormSprite :: Sprite ()
	, userFormInput :: Element
	, userFormSendButton :: Element
	}

mkUserForm :: ScreenSize -> UI UserForm
mkUserForm sz = do
	button <- UI.button 
		& set UI.text "Send" 
		& set UI.style 
			[ ("flex", "1")
			]
	input <- UI.input
	w <- UI.div # set UI.style
		[ ("z-index", "100") -- above all other usual stuff
		, ("display", "flex")
		, ("flex-direction", "row")
		, ("width", show (sayingWidth sz) ++ "px")
		] #+ [element input, element button]
	sprite <- mkSprite () (negate 1000,100) w
	return (UserForm sprite input button)

sayingWidth :: ScreenSize -> Int
sayingWidth sz = screenWidth sz `div` 5

faceSz :: ScreenSize -> Int
faceSz sz = min (screenWidth sz) (screenHeight sz) `div` 15

data BadgeDisplay = BadgeDisplay 
	{ badgeSprite :: Sprite ()
	, badgeName :: Element 
	, badgeBio :: Element
	, badgeButton :: Element
	}

mkBadgeDisplay :: ScreenSize -> UI BadgeDisplay
mkBadgeDisplay sz = do
	button <- UI.button & set UI.text "Close badge"
	name <- UI.div
	bio <- UI.div
	box <- UI.div #+
		[ element button
		, UI.br
		, element name # set UI.align "center"
		, UI.p
		, element bio # set UI.align "center"
		, UI.p
		, UI.img
			& set UI.src conferenceBadgeLogo
			& set UI.align "center"
		]
		& windowStyle sz
		& set UI.style [("display", "none")]
	sprite <- mkSprite () (negate 1000,100) box
	return (BadgeDisplay sprite name bio button)

windowStyle :: ScreenSize -> (UI Element -> UI Element)
windowStyle sz = set UI.style
	[ ("position", "absolute")
	, ("z-index", "999") -- above all
	, ("border", show (faceSz sz `div` 10) ++ "px solid")
	, ("border-color", "#285289")
	, ("background", "white")
	, ("border-radius",show corner ++ "px")
	]
  where
	corner = faceSz sz `div` 5

data MessageWindow = MessageWindow (Sprite ()) Element

messageWindowSprite :: MessageWindow -> Sprite ()
messageWindowSprite (MessageWindow s _) = s

mkMessageWindow :: ScreenSize -> UI MessageWindow
mkMessageWindow sz = do
	b <- UI.button
	box <- UI.div & windowStyle sz
	s <- mkSprite () (negate 1000, 100) box
	return $ MessageWindow s b

mkJitsiMeetLink :: UI Element
mkJitsiMeetLink = UI.a #+ [string "Jitsi meet with your current group"]
	& set UI.href "https://meet.jit.si/"
	& set UI.target "_blank" -- open new window
