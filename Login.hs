module Login where

import UI
import Users
import Config
import Groups

import Graphics.UI.Threepenny.Core
import Text.Read
import Data.List
import Crypto.MAC.HMAC
import Crypto.Hash
import Data.List.Split
import qualified Data.ByteString as S
import qualified Data.ByteString.Char8 as S8
import Network.URI

newtype Secret = Secret S.ByteString

loadSecretFile :: IO Secret
loadSecretFile = Secret <$> S.readFile secretFile

login :: Secret -> UI (Maybe User)
login secret = loginUrl secret <$> getWindowUrl

-- All user state (except icon) is encoded in the url. First is
-- the uid, then a cryptographic proof of the uid, name, and class.
-- Followed by the name, class, and finally the bio.
-- This allows the bio to be changed as desired.
genUrl :: Secret -> String -> Int -> User -> String
genUrl secret hostname port user = 
	"http://" ++ hostname ++ ":" ++ show port ++ "/"
		++ show (fromIntegral (userId user) :: Int) ++ ":"
		++ hmacUserId (userId user) (userName user) (userClass user) secret ++ ":"
		++ userName user ++ ":"
		++ userClass user ++ ":"
		++ userBio user

-- Verifies the proof.
loginUrl :: Secret -> String -> Maybe User
loginUrl secret url = case splitOn ":" url of
	(_http : _host : port_n_u : proof : urlname : uclass : urlrest) -> 
		let u = drop 1 (dropWhile (/= '/') port_n_u)
		    name = unEscapeString urlname
		    bio = intercalate ":" (map unEscapeString urlrest)
		in case UserId <$> readMaybe u of
			Just uid
				| hmacUserId uid name uclass secret == proof ->
					Just (User uid name bio uclass)
				| otherwise -> Nothing
			Nothing -> Nothing
	_ -> Nothing

hmacUserId :: UserId -> String -> String -> Secret -> String
hmacUserId uid name uclass secret = show $
	hmacGetDigest (hmacUserId' uid name uclass secret)

hmacUserId' :: UserId -> String -> String -> Secret -> HMAC SHA256
hmacUserId' (UserId uid) name uclass (Secret secret) =
	hmac (S8.pack (show uid ++ uclass ++ "," ++ "," ++ name)) secret

hmacGroup :: Int -> Group -> Secret -> String
hmacGroup uniquid g secret = show $
	hmacGetDigest (hmacGroup' uniquid g secret)

hmacGroup' :: Int -> Group -> Secret -> HMAC SHA256
hmacGroup' uniquid g (Secret secret) = 
	hmac (S8.pack (show uniquid ++ "," ++ show g)) secret

