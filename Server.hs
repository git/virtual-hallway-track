{-# LANGUAGE LambdaCase #-}

module Server where

import Controller
import Hall
import UI
import Login
import Registration
import Server.State
import Server.Console
import Log
import Config
import Idle

import qualified Graphics.UI.Threepenny as UI
import Graphics.UI.Threepenny.Core
import Control.Monad
import Control.Concurrent.STM
import Control.Concurrent.Async
import System.IO
import qualified Data.ByteString.Char8 as S8

server :: IO ()
server = do
	hSetBuffering stdout LineBuffering
	hSetBuffering stderr LineBuffering
	state <- listenController =<< getControllerHandle
	void $ async $ serverConsole state
	secret <- loadSecretFile
	startGUI defaultConfig
		{ jsPort = Just serverPort
		, jsAddr = Just (S8.pack serverAddr)
		, jsStatic = Just staticDir
		-- , jsCallBufferMode = UI.NoBuffering
		} (start state secret)

start :: ServerState -> Secret -> Window -> UI ()
start state secret window = login secret >>= \case
	Nothing -> registerUser state secret window
	Just ouruser -> do
		liftIO $ logMessage inviteCode $ "login by " ++ show ouruser
		_ <- return window # set UI.title "Virtual Hallway Track"
		sz <- getScreenSize
		lastact <- liftIO mkLastAct
		hall <- initHall window sz ouruser state lastact
		liftIO $ idleLogout window hall state lastact
		maintainHall window sz state secret lastact hall
		liftIO $ atomically $ writeTQueue (toController state) $
			LoginBy ouruser
		return ()
