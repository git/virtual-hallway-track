module Log where

logMessage :: Maybe String -> String -> IO ()
logMessage (Just what) s = putStrLn $ what ++ ": " ++ s
logMessage Nothing s = putStrLn s
