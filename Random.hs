module Random where

import Users
import Groups

import System.Random
import Data.Maybe

-- The user is added to a group, which will split if it got too big.
--
-- The position of the user in the group is randomized.
addUser :: User -> GroupList -> (GroupList, Group)
addUser u gl@(GroupList l) = (gl', g')
  where
	seed = randomSeed gl
	-- Pick one of the groups on the left-most side of the screen.
	-- That way, the user's icon will be visible in the default
	-- viewport. There are two rows of groups, so first 2.
	(groupnum, seed') = randomR (1, min 2 (length l)) seed
	g = if null l then initialGroup else fst (l !! pred groupnum)
	gl' = changeUsersInGroup g (addUser' seed' u) gl
	g' = fromMaybe g (getUserGroup (userId u) gl')

addUser' :: StdGen -> User -> [User] -> [User]
addUser' seed u ul =
	let (a,b) = splitAt (fst (randomR (0, length ul) seed)) ul
	in a++[u]++b

-- Move a user from one group to another. Fails if the user is not in the
-- first group.
moveUser :: UserId -> Group -> Group -> GroupList -> Maybe (GroupList, Group)
moveUser u fromgroup togroup gl = moveUser' u fromgroup togroup gl addUser'

-- Moves a user to be next to another user in a group.
moveUserBeside :: UserId -> Group -> Group -> UserId -> GroupList -> Maybe (GroupList, Group)
moveUserBeside u fromgroup togroup neighbor gl =
	moveUser' u fromgroup togroup gl addf
  where
	addf _seed u' ul
		| any (\v -> userId v == neighbor) ul =
			let (a,b) = break (\v -> userId v == neighbor) ul
			in a ++ [u'] ++ b
		| otherwise = u' : ul

moveUser' :: UserId -> Group -> Group -> GroupList -> (StdGen -> User -> [User] -> [User]) -> Maybe (GroupList, Group)
moveUser' uid fromgroup togroup gl addf = do
	u <- findUserInGroup uid fromgroup gl
	let gl' = changeUsersInGroup fromgroup (filter (/= u)) $
		changeUsersInGroup togroup (addf (randomSeed gl) u) gl
	newgroup <- getUserGroup (userId u) gl'
	return (gl', newgroup)

-- Adds a user to a group, next to another user.
joinGroupBeside :: User -> Group -> UserId -> GroupList -> (GroupList, Group)
joinGroupBeside u togroup neighbor gl = (gl', newgroup)
  where
	gl' = changeUsersInGroup togroup (addf (randomSeed gl) u) gl
	newgroup = fromMaybe togroup (getUserGroup (userId u) gl')
	addf _seed u' ul
		| any (\v -> userId v == neighbor) ul =
			let (a,b) = break (\v -> userId v == neighbor) ul
			in a ++ [u'] ++ b
		| otherwise = u' : ul

-- A number of radians to skew a circle of users.
circleSkew :: [User] -> Double
circleSkew l = fst $ randomR (0, 1) (mkStdGen (userSeed l))

-- Deterministic random seed based on the structure of the groups,
-- and the UserIds.
randomSeed :: GroupList -> StdGen
randomSeed (GroupList l) = mkStdGen $
	groupStructureSeed (map fst l) + userSeed (concatMap snd l)

groupStructureSeed :: [Group] -> Int
groupStructureSeed = mkSeed . concatMap go
  where
	go (Group l) = map splitNum l

groupSeed :: Group -> Int
groupSeed (Group l) = mkSeed (map splitNum l)

userSeed :: [User] -> Int
userSeed = mkSeed . map (fromIntegral . userId)

-- Spread the values out to preserve entropy (probably badly but does not
-- need to be very good).
mkSeed :: [Int] -> Int
mkSeed = go 0 (0 :: Int)
  where
	go x _ [] = x
	go x n (v:vs) = go (x + (v*(10^(n `mod` 10)))) (succ n) vs
