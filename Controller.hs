{-# LANGUAGE BangPatterns, LambdaCase #-}

module Controller (
	module Controller,
	module Controller.Types,
) where

import Controller.State
import Controller.Types
import ControlSocket
import Groups
import Users
import Random
import Threads
import Log
import Config

import Control.Concurrent.STM
import Control.Monad
import Control.Exception
import Data.Maybe
import System.Posix.Files
import qualified Data.Map.Strict as M
import qualified Data.Set as S

-- When the controlSocket exists, connect to the socket.
--
-- If there's no file, but controlSocket is set, run as the controller,
-- in which case this does not ever return!
--
-- Otherwise, runs the controller in its own thread.
getControllerHandle :: IO ControllerHandle
getControllerHandle = case controlSocket of
	Nothing -> startController
	Just socketfile ->
		(try (getFileStatus socketfile) :: IO (Either SomeException FileStatus)) >>= \case
			Left _ -> do
				(ControllerHandle tocontroller fromcontroller) <- startController
				soc <- bindSocket socketfile
				logMessage Nothing "Running as controller. Run again to start a server."
				serveSocket soc tocontroller fromcontroller
				-- never returns
			Right _ -> do
				tocontroller <- newTQueueIO
				fromcontroller <- newTQueueIO
				background "getControllerHandle" $
					connectSocket socketfile tocontroller fromcontroller
				return (ControllerHandle tocontroller fromcontroller)

startController :: IO ControllerHandle
startController = do
	state <- initControllerState
	tocontroller <- newTQueueIO
	background "runController" $ runController state tocontroller
	fromcontroller <- newTQueueIO
	atomically $ do
			cl <- readTVar (clientList state)
			writeTVar (clientList state) (fromcontroller : cl)
	return (ControllerHandle tocontroller fromcontroller)

sendController :: ControllerHandle -> ToController -> IO ()
sendController (ControllerHandle tocontroller _fromcontroller) =
	sendController' tocontroller

sendController' :: TQueue ToController -> ToController -> IO ()
sendController' tocontroller msg = do
	logMessage (Just "controller") $ "> " ++ show msg
	atomically $ writeTQueue tocontroller msg

receiveController :: ControllerHandle -> IO FromController
receiveController (ControllerHandle _tocontroller fromcontroller) =
	atomically $ readTQueue fromcontroller

broadcastClients :: ControllerState -> FromController -> IO ()
broadcastClients state msg = do
	logMessage (Just "controller") $ "< " ++ show msg
	atomically $ do
		l <- readTVar (clientList state)
		forM_ l $ \c -> writeTQueue c msg

updateConnectedUsers :: ControllerState -> UserId -> (Int -> Int) -> ControllerState
updateConnectedUsers state userid f = state 
	{ connectedUsers = M.insert userid v (connectedUsers state)
	}
  where
	v = f (fromMaybe 0 $ M.lookup userid (connectedUsers state))

detectGroupSplit :: ControllerState -> Group -> GroupList -> GroupList -> IO ()
detectGroupSplit state newgroup a b = when (numGroups a /= numGroups b) $
	broadcastClients state (GroupSplit (parentGroup newgroup))

runController :: ControllerState -> TQueue ToController -> IO ()
runController startstate tocontroller = go startstate
  where
	go state = do
		msg <- atomically $ readTQueue tocontroller
		case msg of
			LoginBy user -> checkban (userId user) state $ case getUserGroup (userId user) (groupList state) of
				Just group -> do
					broadcastClients state (LoggedIn user)
					broadcastClients state (GroupNow user group)
					broadcastClients state (UniqueId (uniqueId state))
					let !state' = updateConnectedUsers state (userId user) succ
					let !gl = updateUser user (groupList state)
					let !state'' = state' { groupList = gl }
					go state''
				Nothing -> do
					broadcastClients state (NewUser user)
					let (gl, group) = addUser user (groupList state)
					broadcastClients state (AddUser user)
					broadcastClients state (GroupNow user group)
					detectGroupSplit state group (groupList state) gl
					let !state' = updateConnectedUsers (state { groupList = gl }) (userId user) succ
					broadcastClients state (UniqueId (uniqueId state))
					go state'
			LogoutBy userid -> checkban userid state $ do
				let !state' = updateConnectedUsers state userid pred
				if M.lookup userid (connectedUsers state') < Just 1
					then logout userid state'
					else go state'
			GetGroupList -> do
				broadcastClients state $
					GroupListNow (groupList state)
				go state
			MoveUserBeside u oldgroup newgroup neighbor -> checkban (userId u) state $
				case moveUserBeside (userId u) oldgroup newgroup neighbor (groupList state) of
					Nothing -> go state
					Just (!gl, group) -> do
						let !state' = state { groupList = gl }
						broadcastClients state' $
							MovedUserBeside u oldgroup newgroup neighbor
						broadcastClients state (GroupNow u group)
						detectGroupSplit state group (groupList state) gl
						go state'
			JoinGroupBeside u group neighbor -> checkban (userId u) state $ do
				let (!gl, group') = joinGroupBeside u group neighbor (groupList state)
				let !state' = state { groupList = gl }
				broadcastClients state' $
					JoinedGroupBeside u group neighbor
				broadcastClients state (GroupNow u group')
				detectGroupSplit state group (groupList state) gl
				go state'
			SendMessage userid group content -> checkban userid state $ do
				broadcastClients state (SentMessage userid group content)
				go state
			SendBroadcastMessage s -> do
				broadcastClients state (SentBroadcastMessage s)
				go state
			KickBan userid -> do
				let !state' = state { bannedUsers = S.insert userid (bannedUsers state) }
				logout userid state'

	logout userid state = 
		case leaveGroup userid (groupList state) of
			(gl, Just group) -> do
				let !state' = state { groupList = gl }
				broadcastClients state' $
					LeftGroup userid group
				go state'
			_ -> go state
	
	checkban userid state cont
		| S.member userid (bannedUsers state) = go state
		| otherwise = cont
