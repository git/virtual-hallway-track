module Registration where

import Login
import Config
import UI
import Users
import Image
import Server.State

import qualified Graphics.UI.Threepenny as UI
import Graphics.UI.Threepenny.Core
import Control.Monad
import Data.Function ((&))
import Data.Char
import Data.List
import System.FilePath
import Control.Concurrent.STM

checkInviteCode :: ServerState -> Window -> UI () -> UI ()
checkInviteCode state window cont = case inviteCode of
	Nothing -> cont
	Just code -> do
		allowed <- liftIO $ atomically $
			readTVar (allowRegistration state)
		url <- getWindowUrl
		if takeFileName url == code && allowed
			then cont
			else do
				void $ return window
					& set UI.title "Virtual Hallway Track (Registration)"
				message <- UI.div #+
					[ string "Registration is closed on this server."
					, UI.p
					, string "Maybe if you ask around the conference channels, you can find an invite code for another server."
					]
				_  <- getBody window #+ map element [message]
				return ()

registerUser :: ServerState -> Secret -> Window -> UI ()
registerUser state secret window = checkInviteCode state window $ do
	void $ return window # set UI.title "Virtual Hallway Track (Registration)"
	
	name <- UI.input
	photo <- UI.input
	button <- UI.button & set UI.text "Register"
	status <- UI.span
	mainform <- UI.div #+
		[ UI.img & set UI.src "https://libreplanet.org/w/images/c/ca/Logo_long_alltext.png"
		, UI.p
		, string "Stick on a virtual conference badge so other atendees know who you are."
		, UI.p
		, UI.div #+
			[ string "Please enter your name: "
			, UI.br
			, element name
			]
		, UI.p
		, UI.div #+
			[ string "Enter either an url to a picture for your badge, or your email address: "
			, UI.br
			, element photo
			]
		, UI.div #+
			[ string "(Email address is only used to get an icon from "
			, UI.a #+ [string "libravatar.org"] 
				& set UI.href "https://libravatar.org/"
			, string ")"
			]
			& set UI.target "_blank" -- open new window
		, UI.p
		, element button
		, UI.p
		, element status
		, UI.p
		, UI.a #+ [string "This is a LibrePlanet Safe Space"]
			& set UI.href "https://libreplanet.org/2020/safe-space-policy/" 
			& set UI.target "_blank" -- open new window
		]
	let stylediv d = void $ element d # set UI.style
		[ ("position", "absolute")
		, ("top", "10px")
		, ("width", "90%")
		, ("left", "5%")
		, ("padding", "20px")
		, ("background", "lightgreen")
		]
	stylediv mainform

	footer <- UI.div #+
		[ UI.p
		, UI.div #+
			[ UI.a #+ [string "virtual-hallway-track"]
				& set UI.href gitRepoUrl
				& set UI.target "_blank" -- open new window
			, string " is free software licensed under the terms of the GNU AGPL."
			]
		, UI.p
		, UI.div #+
			[ string "(Written by "
			, UI.a #+ [string "Joey Hess"]
				& set UI.href "https://joeyh.name/"
				& set UI.target "_blank" -- open new window
			, string " in 4 days when LibrePlanet 2020 went virtual.)"
			]
		]
	void $ element footer # set UI.style
		[ ("position", "absolute")
		, ("bottom", "10px")
		, ("width", "50%")
		, ("padding", "20px")
		, ("left", "25%")
		, ("background", "lightgrey")
		]

	_  <- getBody window #+ map element [mainform, footer]
	
	let regcomplete url = do
		-- New window needed because the js running in this one
		-- reacts to changing url by reloading.
		d <- UI.div #+
			[ UI.img & set UI.src "https://libreplanet.org/w/images/c/ca/Logo_long_alltext.png"
			, UI.p
			, string "Registration is complete."
			, UI.br
			, UI.a #+ [string "Click here to open the Virtual Hallway Track"]
				& set UI.href url
				& set UI.target "_blank" -- open new window
			, UI.p
			, string "(Bookmark that link so you can return in between conference presentations.)"
			]
		stylediv d
		_  <- getBody window #+ [element d]
		void $ element mainform # set style [("display", "none")]

	let finish _ = do
		n <- UI.get UI.value name
		p <- UI.get UI.value photo
		if null n || all isSpace n || null p
			then void $ element status
				& set UI.text "Invalid input."
			else do
				void $ element status
					& set UI.text "Downloading your picture...."
				v <- liftIO $ storeUserImage $ if "http" `isPrefixOf` p
					then downloadAndScaleUrl p
					else downloadLibravatar p 
				case v of
					Nothing -> void $ element status
						& set UI.text "Was not able to download your picture. Try another url."
					Just uid -> do
						let user = User uid n "" ""
						let url = genUrl secret serverAddr serverPort user
						regcomplete url
	
	on UI.click button finish
	UI.setFocus name
	on UI.sendValue name $ \_ -> UI.setFocus photo
	on UI.sendValue photo finish
