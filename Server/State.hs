{-# LANGUAGE BangPatterns, LambdaCase #-}

module Server.State where

import Groups
import Users
import Random
import Controller
import Threads
import Log
import Config

import Control.Concurrent.STM
import Graphics.UI.Threepenny.Core
import Control.Monad

data ServerState = ServerState
	{ groupList :: TVar GroupList
	-- ^ maintained in one place for all clients, to avoid multiple
	-- copies in memory and excess work
	, relayTo :: TVar [TQueue (Maybe FromController)]
	-- ^ where to relay messages received from the controller
	, toController :: TQueue ToController
	, allowRegistration :: TVar Bool
	, uniqueId :: TMVar Int
	}

-- Starts a thread that listens to the controller, updates the
-- ServerState as messages are received, and relays messages on to other
-- consumers.
listenController :: ControllerHandle -> IO ServerState
listenController ch@(ControllerHandle tocontroller _) = do
	-- This is expensive, but is only done when a new server starts up.
	-- Other changes to the group list are sent incrementally, rather
	-- than needing to send the whole list over the wire.
	sendController ch GetGroupList
	waitlist
  where
	waitlist = receiveController ch >>= \case
		GroupListNow gl -> do
			state <- atomically $ ServerState
				<$> newTVar gl
				<*> newTVar []
				<*> pure tocontroller
				<*> newTVar True
				<*> newEmptyTMVar
			background "listenController" $ listenchanges state
			return state
		_ -> waitlist
	
	listenchanges state = do
		msg <- receiveController ch
		case msg of
			NewUser _ -> return ()
			GroupNow _ _ -> return ()
			GroupSplit _ -> return ()
			SentMessage _ _ _ -> return ()
			SentBroadcastMessage _ -> return ()
			-- Since the random seed is deterministic,
			-- we'll get the same result performing these
			-- actions as the controller did.
			MovedUserBeside u oldgroup newgroup neighbor -> 
				modgrouplist $ fmap fst 
					. moveUserBeside (userId u) oldgroup newgroup neighbor
			JoinedGroupBeside u group neighbor -> 
				modgrouplist $ Just . fst
					. joinGroupBeside u group neighbor
			GroupListNow gl -> atomically $
				writeTVar (groupList state) gl
			AddUser u -> modgrouplist $ Just . fst . addUser u
			LeftGroup u _ -> modgrouplist $ Just . fst . leaveGroup u
			LoggedIn u -> modgrouplist $ Just . updateUser u
			UniqueId n -> do
				logMessage inviteCode $ "unique id: " ++ show n
				atomically $ do
					_ <- tryTakeTMVar (uniqueId state)
					putTMVar (uniqueId state) n

		atomically $ do
			l <- readTVar (relayTo state)
			forM_ l $ \c -> writeTQueue c (Just msg)
		listenchanges state
	  where
		modgrouplist f = do
			gl <- atomically $ do
				gl <- readTVar (groupList state)
				case f gl of
					Just gl' -> do
						writeTVar (groupList state) gl'
						return gl'
					Nothing -> return gl
			liftIO $ logMessage inviteCode $ "group list now: " ++ show gl

relayFromController :: ServerState -> Window -> UserId -> (TQueue (Maybe FromController) -> IO ()) -> UI ()
relayFromController state window ouruserid a = do
	c <- liftIO $ atomically $ do
		l <- readTVar (relayTo state)
		q <- newTQueue
		writeTVar (relayTo state) (q:l)
		return q
	let cleanup = liftIO $ do
		atomically $ do
			-- tell consumer there is nothing more on this queue
			writeTQueue c Nothing
			l <- readTVar (relayTo state)
			let !l' = filter (/= c) l
			writeTVar (relayTo state) l'
		sendController' (toController state) (LogoutBy ouruserid)
		logMessage inviteCode $ "client exited cleanly"
			
	liftIO $ background ("relayFromController " ++ show ouruserid) (a c)
	_ <- onEvent (disconnect window) $ \_ -> cleanup
	return ()
