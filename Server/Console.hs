module Server.Console where

import Server.State
import Users
import Controller
import Log
import Config

import Control.Monad
import Control.Concurrent.STM
import Text.Read

data ConsoleCommands
	= CloseRegistration
	| OpenRegistration
	| KickBanUser UserId
	deriving (Read, Show, Eq)

serverConsole :: ServerState -> IO ()
serverConsole state = forever $ do
	cmd <- getLine
	case readMaybe cmd of
		Nothing -> logMessage inviteCode "invalid server console command"
		Just CloseRegistration -> atomically $
			writeTVar (allowRegistration state) False
		Just OpenRegistration -> atomically $
			writeTVar (allowRegistration state) True
		Just (KickBanUser uid) -> do
			sendController' (toController state) (KickBan uid)
