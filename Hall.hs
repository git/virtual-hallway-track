{-# LANGUAGE BangPatterns, LambdaCase #-}

module Hall where

import Hall.Types
import Config
import Users
import Groups
import UI
import Random
import Controller
import Server.State
import Login
import Idle.Types
import Idle

import qualified Graphics.UI.Threepenny as UI
import Graphics.UI.Threepenny.Core
import Control.Concurrent.STM
import Control.Monad
import Data.Function ((&))
import qualified Data.Map.Strict as M
import Data.Maybe
import Data.Char
import System.Random

deleteUserElt :: UserElt -> UI ()
deleteUserElt (UserElt sprite other _) = do
	deleteSprite sprite
	maybe (return ()) deleteSprite other

deleteSprite :: Sprite t -> UI ()
deleteSprite = UI.delete . spriteElement

-- Listens for events and updates the hall's display accordingly.
-- Runs in the background.
maintainHall :: Window -> ScreenSize -> ServerState -> Secret -> LastAct -> TVar Hall -> UI ()
maintainHall window sz state secret lastact hall = do
	h <- liftIO $ atomically $ readTVar hall
	relayFromController state window (userId (ourUser h)) $
		loop (userId (ourUser h)) (ourGroup h)
  where
	loop ouruserid ourgroup ch = do
		msg <- atomically (readTQueue ch)
		let cont = loop ouruserid ourgroup ch
		case msg of
			Just (SentMessage u g content) -> do
				when (Just g == ourgroup) $
					messageinourgroup u content
				cont
			Just (NewUser u) -> do
				runUI window $ addelt u
				cont
			Just (GroupNow u newg) -> do
				groupupdate (userId u) (Just newg)
				if ouruserid == userId u
					then loop ouruserid (Just newg) ch
					else cont
			Just (AddUser _) -> cont
			Just (LoggedIn _) -> cont
			Just (UniqueId _) -> cont
			Just (MovedUserBeside {}) -> cont
			Just (JoinedGroupBeside {}) -> cont
			Just (GroupSplit splitg)
				| (Just splitg == ourgroup) -> do
					gl <- liftIO $ atomically $
						readTVar (groupList state)
					let newg = getUserGroup ouruserid gl
					groupupdate ouruserid newg
					loop ouruserid newg ch
				| otherwise -> cont
			Just (LeftGroup uid _) -> do
				groupupdate uid Nothing
				gl <- liftIO $ atomically $ readTVar (groupList state)
				when (isNothing $ getUserGroup uid gl) $ do
					removeelt uid >>=
						runUI window . mapM_ deleteUserElt
				cont
			-- Not yet used.
			Just (SentBroadcastMessage _msg) -> cont
			-- Don't need to handle this; all changes are sent
			-- incrementally.
			Just (GroupListNow _) -> cont
			Nothing -> return ()
	  where
		shrinking = map
			(\p -> \elt -> elt & set UI.style [("font-size", show p++"%")])
			(([100,80,70] ++ [50..]) :: [Int])
		messageinourgroup u content = do
			v <- atomically $ do
				h <- readTVar hall
				case M.lookup u (userElts h) of
					Just (UserElt s (Just elt) history)-> do
						let history' = take numOldMessages (content:history)
						let m = M.insert u (UserElt s (Just elt) history') (userElts h)
						writeTVar hall (h { userElts = m })
						return (Just (elt, history'))
					_ -> return Nothing
			case v of
				Nothing -> return ()
				Just (elt, history) -> runUI window $ do
					cs <- mapM id $ concatMap
						(\(l, s) -> [s (string l), UI.br])
						(zip history shrinking)
					void $ element (spriteElement elt)
						& set UI.children cs
					showSprite elt
		groupupdate uid newg = runUI window $ do
			-- When we change groups, clear our display of our
			-- own history, and of what we said last, since it
			-- was not said to this group. But not when
			-- the group split.
			--
			-- (We keep what other people said buffered,
			-- because it can be convenient to come back to a
			-- group and have the history of a conversation.)
			--
			-- Also update the UI.
			when (ouruserid == uid) $ do
				let groupsplit = fromMaybe False $ do
					n <- newg
					o <- ourgroup
					return $ parentGroup n == o
				h <- liftIO $ atomically $ do
					h <- readTVar hall
					let !h' = h { ourGroup = newg }
					let h'' = if groupsplit
						then h'
						else case M.lookup ouruserid (userElts h) of
							Just (UserElt s elt _history) ->
								let !m = M.insert ouruserid (UserElt s elt []) (userElts h')
								in h' { userElts = m }
							Nothing -> h'
					writeTVar hall h''
					return h
				when (not groupsplit) $ do
					case M.lookup uid (userElts h) of
						Just (UserElt _ (Just elt) _) -> void $
							element (spriteElement elt)
								& set UI.text ""
						_ -> return ()
				updateJitsiMeetLink h state secret
			updateHallDisplay hall state sz
		addelt u = do
			sprite <- mkSprite (userId u) (negate 1000,0)
				=<< loadImage u (faceSz sz)
			saying <- mkSprite (userId u) (negate 1000,0)
				=<< mkSaying sz (ouruserid == userId u)
			when (userId u /= ouruserid) $
				hideSprite saying
			_  <- getBody window #+ map (element . spriteElement) [sprite, saying]
			addClickHandler sprite hall state sz lastact
			liftIO $ atomically $ do
				h <- readTVar hall
				let userelt = UserElt sprite (Just saying) []
				let !h' = h { userElts = M.insert (userId u) userelt (userElts h) }
				writeTVar hall h'
		removeelt uid = liftIO $ atomically $ do
			h <- readTVar hall
			let !(deadelts, liveelts) = 
				M.partitionWithKey
					(\uid' _ -> uid' == uid)
					(userElts h)
			let !h' = h { userElts = liveelts }
			writeTVar hall h'
			return (M.elems deadelts)

-- Initializes and displays the hall.
initHall :: Window -> ScreenSize -> User -> ServerState -> LastAct -> UI (TVar Hall)
initHall window sz ouruser state lastact = do
	grouplist <- liftIO $ atomically $ readTVar (groupList state)
	sprites <- forM (usersInGroupList grouplist) $ \u -> 
		mkSprite (userId u) (0,0) =<< loadImage u (faceSz sz)
	sayings <- forM (usersInGroupList grouplist) $ \u -> do
		s <- mkSprite (userId u) (0,0) =<< mkSaying sz (userId u == userId ouruser)
		when (userId u /= userId ouruser) $
			hideSprite s
		return s
	userform <- mkUserForm sz
	badgedisplay <- mkBadgeDisplay sz
	jitsimeetlink <- mkJitsiMeetLink
	messagewindow <- mkMessageWindow sz

	hall <- liftIO $ newTVarIO $ Hall
		{ ourUser = ouruser
		, ourGroup = Nothing
		, userElts = M.fromList $ map (\elt -> (userEltId elt, elt)) $
			map (\(us, ss) -> UserElt us ss []) $
				zip sprites (map Just sayings)
		, userForm = userform
		, badgeDisplay = badgedisplay
		, jitsiMeetLink = jitsimeetlink
		, messageWindow = messagewindow
		}
	updateHallDisplay hall state sz

	upperwall <- mkWall True sz
	lowerwall <- mkWall False sz

	logo <- UI.img
		& set UI.src conferenceLogo
		& set UI.style
			[ ("z-index", "1") -- above wall
			, ("position", "absolute")
			, ("top", "0px")
			, ("left", "0px")
			, ("height", show (wallHeight sz - 2) ++ "px")
			]
	
	let footercolor = set UI.style [("color", "lightgrey")]
	help <- UI.div #+ 
		[ string "Click on a person to join their group or see their badge." & footercolor
		, UI.br
		, element jitsimeetlink & footercolor
		]
		& set UI.style
			[ ("z-index", "1") -- above wall
			, ("position", "fixed")
			, ("bottom", "5px")
			, ("left", "5px")
			]
	policy <- UI.div #+
		[ UI.a #+ [string "This is a LibrePlanet Safe Space"]
			& set UI.href "https://libreplanet.org/2020/safe-space-policy/" 
			& footercolor
		]
		& set UI.target "_blank" -- open new window
		& set UI.style
			[ ("z-index", "1") -- above wall
			, ("position", "fixed")
			, ("bottom", "5px")
			, ("right", "5px")
			]

	let elts = map element $ spriteElement (badgeSprite badgedisplay)
		: spriteElement (messageWindowSprite messagewindow)
		: logo : help : policy : upperwall : lowerwall
		: spriteElement (userFormSprite userform) 
		: (map spriteElement (sprites++sayings))
	_  <- getBody window #+ elts
	forM_ sprites $ \s -> 
		addClickHandler s hall state sz lastact
	
	UI.setFocus (userFormInput userform)
	let sendmessage content = do
		liftIO $ sendMessage content hall state
		void $ element (userFormInput userform) # UI.set UI.value ""
		notIdle lastact
	on UI.sendValue (userFormInput userform) sendmessage
	on UI.click (userFormSendButton userform) $ \_ -> do
		content <- UI.get UI.value (userFormInput userform)
		sendmessage content

	return hall

sendMessage :: String -> TVar Hall -> ServerState -> IO ()
sendMessage content hall state
	| all isSpace content = return ()
	| otherwise = do
		h <- atomically $ readTVar hall
		case ourGroup h of
			Nothing -> return ()
			Just g -> sendController' (toController state) $
				SendMessage (userId (ourUser h)) g content

updateHallDisplay :: TVar Hall -> ServerState -> ScreenSize -> UI ()
updateHallDisplay hall state sz = do
	(GroupList l, h) <- liftIO $ atomically $ (,)
		<$> readTVar (groupList state)
		<*> readTVar hall
	go 0 l h
	UI.setFocus (userFormInput (userForm h))
  where
	go _ [] _ = return ()
	go n ((group, users):rest) h = do
		let isourgroup = Just group == ourGroup h
		let rng = mkStdGen (groupSeed group)
		let skew = circleSkew users
		let maxwobble = faceSz sz `div` 5
		let wobbles = map (+) (randomRs (negate maxwobble, maxwobble) rng)
		let addwobble l = zip l (zip wobbles (drop (length users) wobbles))
		let groupelts = mapMaybe (flip M.lookup (userElts h) . userId) users
		let radius = faceSz sz * 2 * 13 `div` 14
		let halfn = n `div` 2
		let xadj = fst $ randomR (negate (radius `div` 2), radius `div` 2) rng
		let yadj = fst $ randomR (0, radius `div` 2) rng
		let wallheight = screenHeight sz `div` 10
		let x = (radius*(2+halfn*6) `div` 2)+(halfn*screenWidth sz `div` 25)+(radius*4)+xadj
		let y = if odd n
			then screenHeight sz - radius - (wallheight * 2) - yadj
			else radius + wallheight + yadj
		_ <- arrangeCircle radius ((x +), (y +)) skew $
			addwobble (map userSprite groupelts)

		let sayingxpos = if isourgroup
			then (\v -> if v < 0 
				then x + v - sayingWidth sz + faceSz sz
				else x + v)
			else (\v -> v - 10000)
		_ <- arrangeCircle (max (faceSz sz) (radius * 5 `div` 3)) (sayingxpos, (y +)) skew $
			addwobble (mapMaybe userSaying groupelts)

		when isourgroup $
			case M.lookup (userId (ourUser h)) (userElts h) of
				Just (UserElt _ (Just oursaying) _) -> do
					(sx,sy) <- spritePos oursaying
					void $ moveTo (sx,sy + faceSz sz)
						(userFormSprite (userForm h))
				_ -> return ()

		go (succ n) rest h

addClickHandler :: Sprite UserId -> TVar Hall -> ServerState -> ScreenSize -> LastAct -> UI ()
addClickHandler s hall state sz lastact = on UI.click (spriteElement s) $ \_ -> do
	notIdle lastact
	join $ liftIO $ atomically $ do
		gl <- readTVar (groupList state)
		h <- readTVar hall
		if userId (ourUser h) == spriteVal s
			then return $ showbadge gl h
			else case getUserGroup (spriteVal s) gl of
				Nothing -> return $ showbadge gl h
				Just newg -> case ourGroup h of
					Just oldg
						| newg == oldg -> return $
							showbadge gl h
						| otherwise -> return $ sendmsg $
							movegroup oldg newg h
					Nothing -> return $ sendmsg $
						joingroup newg h
  where
	sendmsg = liftIO . sendController' (toController state)
	movegroup oldg newg h = MoveUserBeside (ourUser h) oldg newg (spriteVal s)
	joingroup newg h = JoinGroupBeside (ourUser h) newg (spriteVal s)
	showbadge gl h = case findUser (spriteVal s) gl of
		Nothing -> return ()
		Just u -> do
			void $ element (badgeName (badgeDisplay h))
				& set UI.text (userName u)
			void $ element (badgeBio (badgeDisplay h))
				& set UI.text (userBio u)
			let button = badgeButton (badgeDisplay h)
			let bs = badgeSprite (badgeDisplay h)
			(x,y) <- spritePos s
			void $ moveTo (x+faceSz sz,y+faceSz sz) bs
			showSprite bs
			UI.setFocus button
	
			on UI.click button $ \_ -> do
				hideSprite bs
				UI.setFocus (userFormInput (userForm h))
				notIdle lastact
	
updateJitsiMeetLink :: Hall -> ServerState -> Secret -> UI ()
updateJitsiMeetLink h state secret = do
	uniqueid <- liftIO $ atomically $
		tryReadTMVar (uniqueId state)
	let url = "https://meet.jit.si/virtual-hallway-track-" ++
		hmacGroup (fromMaybe 0 uniqueid) (fromMaybe initialGroup (ourGroup h)) secret
	void $ element (jitsiMeetLink h)
		& set UI.href url
