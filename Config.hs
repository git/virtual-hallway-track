module Config where

import Data.Time.Clock

-- If this is set, it must be appended to the url in order to register.
inviteCode :: Maybe String
inviteCode = Nothing

-- If this is set, the controller runs as a separate process than the UI
-- server. That allows multiple UI servers to be run. The first time
-- the program is run, it will serve as the controller, and subsequent
-- times will run as servers.
--
-- For development, using Nothing makes the controller run in the same
-- process, which is more convenient.
controlSocket :: Maybe FilePath
controlSocket = Nothing -- Just "control.sck"

gitRepoUrl :: String
gitRepoUrl = "https://git.joeyh.name/index.cgi/virtual-hallway-track.git/"

conferenceLogo :: String
conferenceLogo = "https://libreplanet.org/w/skins/Tweeki/custom/lp-logo.svg"

conferenceBadgeLogo :: String
conferenceBadgeLogo = "https://static.fsf.org/nosvn/libreplanet/2020/assets/logo-lores.png"

staticDir :: FilePath
staticDir = "./static"

-- File containing some secret content, used to verify login urls.
secretFile :: FilePath
secretFile = "./secretfile"

serverPort :: Int
serverPort = 8023

serverAddr :: String
serverAddr = "127.0.0.1"

-- How many old messages to keep in the scrollback, besides a user's
-- latest.
numOldMessages :: Int
numOldMessages = 5

-- How long can a user be idle before being logged out?
maxIdle :: NominalDiffTime
maxIdle = 600

-- How many bots to seed with at start?
numBots :: Int
numBots = 15

-- How big can a group get before it splits?
maxGroupSize :: Int
maxGroupSize = 8

-- Groups larger than this will split eventually.
-- (not actually implemented yet)
optimalGroupSize :: Int
optimalGroupSize = maxGroupSize - 2
