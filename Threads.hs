module Threads where

import Log
import Config

import Control.Concurrent.Async
import Control.Monad

background :: String -> IO () -> IO ()
background desc a = void $ async $ do
	t <- async a
	res <- waitCatch t
	case res of
		Right () -> logMessage inviteCode $ desc ++ " thread exited"
		Left e -> logMessage inviteCode$ desc ++ " thread crashed " ++ show e
