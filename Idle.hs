module Idle where

import Idle.Types
import Config
import Controller
import Hall.Types
import Server.State
import UI
import Threads
import Users

import qualified Graphics.UI.Threepenny as UI
import Graphics.UI.Threepenny.Core
import Control.Concurrent
import Control.Concurrent.STM
import Data.Time.Clock.POSIX
import Control.Monad
import Data.Function ((&))
import qualified Data.Map as M

mkLastAct :: IO LastAct
mkLastAct = do
	now <- getPOSIXTime
	LastAct <$> newTVarIO now

notIdle :: LastAct -> UI ()
notIdle (LastAct v) = liftIO $ do
	now <- getPOSIXTime
	atomically $ writeTVar v now

waitActivityAfter :: LastAct -> POSIXTime -> IO ()
waitActivityAfter (LastAct v) t = go
  where
	go = do
		threadDelay 60000000 -- 60s
		lastact <- atomically $ readTVar v
		if lastact > t
			then return ()
			else go

-- Runs in the background.
idleLogout :: Window -> TVar Hall -> ServerState -> LastAct -> IO ()
idleLogout window hall state l@(LastAct v) = background "idleLogout" $
	go
  where
	go = do
		threadDelay 60000000 -- 60s
		lastact <- atomically $ readTVar v
		now <- getPOSIXTime
		when (now - lastact > maxIdle) $ do
			h <- atomically $ readTVar hall
			let uid = userId (ourUser h)
			sendController' (toController state) (LogoutBy uid)
			let MessageWindow messagewindow button = messageWindow h
			let us = fmap userSprite $ M.lookup uid $ userElts h
			runUI window $ do
				void $ element (spriteElement messagewindow)
					& set UI.children []
				void $ element (spriteElement messagewindow) #+
					[ UI.p
					, string "You have stepped away from the hallway track,"
					, UI.br
					, string "perhaps to watch a talk.."
					, UI.p
					, element button
						& set UI.text "I'm back"
						& set UI.align "center"
					, UI.p
					]
				pos <- maybe (pure (0,0)) spritePos us
				liftIO $ print pos
				void $ moveTo pos messagewindow
				hideSprite (userFormSprite (userForm h))
				showSprite messagewindow
				UI.setFocus button
				on UI.click button $ \_ -> do
					hideSprite messagewindow
					showSprite (userFormSprite (userForm h))
					UI.setFocus (userFormInput (userForm h))
					liftIO $ sendController'
						(toController state)
						(LoginBy (ourUser h))
					notIdle l
			-- Wait for the user to log back in.
			waitActivityAfter l lastact
		go
