module Controller.State where

import Users
import Groups
import Config
import Random
import Controller.Types
import System.Random

import Control.Concurrent.STM
import qualified Data.Map as M
import qualified Data.Set as S

data ControllerState = ControllerState
	{ groupList :: GroupList 
	, connectedUsers :: M.Map UserId Int
	, bannedUsers :: S.Set UserId
	, clientList :: TVar [TQueue FromController]
	, uniqueId :: Int
	}

initControllerState :: IO ControllerState
initControllerState = ControllerState gl M.empty S.empty
	<$> newTVarIO []
	<*> getStdRandom random
  where
	gl :: GroupList
	gl = foldl (\g b -> fst (addUser b g)) newGroupList $ take numBots botStream
