module Controller.Types where

import Users
import Groups

import Control.Concurrent.STM.TQueue

data ControllerHandle = ControllerHandle
	(TQueue ToController)
	(TQueue FromController)

data ToController
	= GetGroupList
	| LoginBy User
	| LogoutBy UserId -- note that user may still have other windows open
	| MoveUserBeside User Group Group UserId
	| JoinGroupBeside User Group UserId
	| SendMessage UserId Group String
	| SendBroadcastMessage String
	| KickBan UserId
	deriving (Show, Read)

data FromController
	= GroupListNow GroupList
	| LoggedIn User
	| NewUser User
	| AddUser User
	| MovedUserBeside User Group Group UserId
	| JoinedGroupBeside User Group UserId
	| GroupNow User Group
	| LeftGroup UserId Group
	| SentMessage UserId Group String
	| GroupSplit Group
	| SentBroadcastMessage String
	| UniqueId Int
	deriving (Show, Read)
