module ControlSocket where

import Controller.Types
import Log
import Config

import System.IO
import qualified Network.Socket as S
import Control.Concurrent.STM
import Control.Concurrent.Async
import Control.Monad
import Text.Read

serveSocket :: S.Socket -> TQueue ToController -> TQueue FromController -> IO a
serveSocket s tocontroller fromcontroller = do
	relay <- newTVarIO []
	_ <- serve relay `race` sendrelay relay
	error $ "serveSocket exited unexpectedly"
  where
	serve :: TVar [TQueue FromController] -> IO ()
	serve relay = do
		fromrelay <- atomically $ do
			l <- readTVar relay
			q <- newTQueue
			writeTVar relay (q:l)
			return q
		(sconn, _) <- S.accept s
		conn <- S.socketToHandle sconn ReadWriteMode
		_ <- async $ sendToConn conn fromrelay
			`race` receiveFromConn conn tocontroller
		serve relay
	sendrelay relay = do
		(msg, rs) <- atomically $ (,)
			<$> readTQueue fromcontroller
			<*> readTVar relay
		forM_ rs $ \r -> atomically $ writeTQueue r msg
		sendrelay relay

connectSocket :: FilePath -> TQueue ToController -> TQueue FromController -> IO ()
connectSocket socketfile tocontroller fromcontroller = do
	soc <- S.socket S.AF_UNIX S.Stream S.defaultProtocol
	S.connect soc (S.SockAddrUnix socketfile)
	conn <- S.socketToHandle soc ReadWriteMode
	_ <- sendToConn conn tocontroller
		`race` receiveFromConn conn fromcontroller
	logMessage inviteCode "lost connection to controller"

sendToConn :: Show t => Handle -> TQueue t -> IO ()
sendToConn conn q = do	
	v <- atomically (readTQueue q)
	hPutStrLn conn (show v)
	hFlush conn
	sendToConn conn q

receiveFromConn :: Read t => Handle -> TQueue t -> IO ()
receiveFromConn conn q = withLines conn go
  where
	go [] = return ()
	go (l:ls)
		| null l = go ls
		| otherwise = case readMaybe l of
			Nothing -> do
				logMessage Nothing ("controller message parse error: " ++ l)
				go ls
			Just v -> do
				atomically $ writeTQueue q v
				go ls

withLines :: Handle -> ([String] -> IO a) -> IO a
withLines conn a =  do
	ls <- lines <$> hGetContents conn
	a ls

bindSocket :: FilePath -> IO S.Socket
bindSocket socketfile = do
	soc <- S.socket S.AF_UNIX S.Stream S.defaultProtocol
	S.bind soc (S.SockAddrUnix socketfile)
	S.listen soc 5
	return soc
