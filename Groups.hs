module Groups where

import Users
import Config

import Data.Maybe

-- Unique identifier for a group. As a group grows, it splits into smaller
-- groups, which are created by adding a SplitId to the list.
--
-- This structure means that the Ord instance keeps closely related
-- subgroups together.
data Group = Group [SplitId]
	deriving (Eq, Ord, Show, Read)

-- The number of constructors here determines how many smaller groups a
-- large group will split into.
data SplitId = A | B
	deriving (Eq, Ord, Show, Read, Enum, Bounded)

splitNum :: SplitId -> Int
splitNum A = 1
splitNum B = 2

-- Splits a group. Note that the resulting list is already sorted,
-- and replacing the old group with it in a sorted list of groups will
-- keep that list sorted as well.
splitGroup :: Group -> [Group]
splitGroup (Group l) = map mk [minBound..maxBound]
  where
	mk sid = Group (l++[sid])

parentGroup :: Group -> Group
parentGroup (Group l) = Group (reverse (drop 1 (reverse l)))

splitGroupUsers :: (Group, [User]) -> [(Group, [User])]
splitGroupUsers (g, ul) = zip gs uls
  where
	gs = splitGroup g
	uls = splitUserList (length gs) ul

-- The group list is always kept sorted.
newtype GroupList = GroupList [(Group, [User])]
	deriving (Show, Read, Eq)

newGroupList :: GroupList
newGroupList = GroupList [(initialGroup, [])]

initialGroup :: Group
initialGroup = Group []

groupSizes :: GroupList -> [Int]
groupSizes (GroupList l) = map (length . snd) l

numGroups :: GroupList -> Int
numGroups (GroupList l) = length l

mapGroups :: ((Group, [User]) -> a) -> GroupList -> [a]
mapGroups f (GroupList l) = map f l

usersInGroupList :: GroupList -> [User]
usersInGroupList = concat . mapGroups snd

findUserInGroup :: UserId -> Group -> GroupList -> (Maybe User)
findUserInGroup uid g (GroupList l) = listToMaybe $
	filter (\u -> userId u == uid) $ concatMap snd $
		filter (\(g', _) -> g' == g) l

findUser :: UserId -> GroupList -> Maybe User
findUser uid (GroupList l) = listToMaybe $
	filter (\u -> userId u == uid) (concatMap snd l)

getUserGroup :: UserId -> GroupList -> Maybe Group
getUserGroup u (GroupList l) = listToMaybe $ mapMaybe go l
  where
	go (g, ul)
		| any (\v -> u == userId v) ul = Just g
		| otherwise = Nothing

splitGroupInList :: Group -> GroupList -> GroupList
splitGroupInList g (GroupList l) = GroupList (go l)
  where
	go [] = []
	go (v@(og, _ul):xs)
		| og == g = splitGroupUsers v ++ go xs
		| otherwise = v : go xs

-- Applies some transformation to the users of a group.
-- Splits the group if it becomes larger than maxGroupSize.
changeUsersInGroup :: Group -> ([User] -> [User]) -> GroupList -> GroupList
changeUsersInGroup g f (GroupList l) = GroupList (concatMap go l)
  where
	go v@(g', us)
		| g' == g = 
			let ul = f us
			in if length ul > maxGroupSize
				then splitGroupUsers (g, ul)
				else [(g, ul)]
 		| otherwise = [v]

-- Replaces any user in the group list that has the same UserId with
-- this new version.
updateUser :: User -> GroupList -> GroupList
updateUser u (GroupList l) = GroupList (map go l)
  where
	go (g, us) = (g, map replace us)
	replace u'
		| userId u' == userId u = u
		| otherwise = u'

leaveGroup :: UserId -> GroupList -> (GroupList, Maybe Group)
leaveGroup uid gl = case getUserGroup uid gl of
	Nothing -> (gl, Nothing)
	Just g -> (changeUsersInGroup g (filter (\u -> userId u /= uid)) gl, Just g)
