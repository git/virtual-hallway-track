{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Users where

data User = User
	{ userId :: UserId
	, userName :: String
	, userBio :: String
	, userClass :: String
	}
	deriving (Eq, Ord, Show, Read)

-- Some fields of User may be changed at runtime, so using UserId
-- for manipulations of the GroupList (other than adding new users)
-- lets two versions of the same user be treated as the same.
newtype UserId = UserId Integer
	deriving (Eq, Ord, Num, Integral, Real, Enum, Show, Read)

botStream :: [User]
botStream = map go [-1,-2..]
  where
	go n = User (UserId n) ("bot" ++ show n) "(GNU logo copyright 2003 FSF, CC-BY-SA-2.0 license)" ""

splitUserList :: Int -> [User] -> [[User]]
splitUserList n l = go [] l
  where
	newsz = max 1 (length l `div` n)
	go c [] = c
	go c l'
		| length c + 1 >= n = l' : c
		| otherwise = 
			let (us, rest) = splitAt newsz l'
			in go (us:c) rest
