module Idle.Types where

import Control.Concurrent.STM
import Data.Time.Clock.POSIX

newtype LastAct = LastAct (TVar POSIXTime)
