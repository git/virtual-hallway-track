module Hall.Types where

import Users
import Groups
import UI

import Graphics.UI.Threepenny.Core
import qualified Data.Map.Strict as M

-- Information we need to remember about the hall, for each user on the
-- server.
data Hall = Hall
	{ ourUser :: User
	, ourGroup :: Maybe Group
	, userElts :: M.Map UserId UserElt
	, userForm :: UserForm
	, badgeDisplay :: BadgeDisplay
	, jitsiMeetLink :: Element
	, messageWindow :: MessageWindow
	}

-- A user's sprite and associated stuff.
data UserElt = UserElt
	{ userSprite :: Sprite UserId
	, userSaying :: Maybe (Sprite UserId)
	, userHistory :: [String]
	}

userEltId ::UserElt -> UserId
userEltId (UserElt s _ _) = spriteVal s
